import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 */

/**
 * @author Tracy
 *
 */
public class Game{

	private List<Card> cardList ;
	private  List<ArrayList<Card>> setList;
	private  List<ArrayList<Card>> runList ;
	private  List<Card> leftOver;
	private Map<Integer,ArrayList<Card>> sets;
	private Map<String,ArrayList<Card>> runs;
	private int runTotal;
	private Set<Card> leftOvers ;

	public Game(){

		cardList = new ArrayList<Card>();
		setList = new ArrayList<ArrayList<Card>>();
		runList = new ArrayList<ArrayList<Card>>();
		leftOver = new ArrayList<Card>();
		sets = new HashMap<Integer,ArrayList<Card>>();
		runs = new HashMap<String,ArrayList<Card>>();
		leftOvers = new HashSet();
		setup();
	}

	public void start(String input){
		getInput(input);
		getSets((ArrayList<Card>) cardList);
		getRuns(((ArrayList<Card>) cardList));
		leftOvers.addAll(leftOver);
		System.out.println("The sets are" + setList.toString());
		System.out.println("The runs are" + runList.toString());
		//System.out.println("Leftovers are" + leftOvers.toString());



	}
	private void setup(){

		//fill order hash map
		int i = 1;
		while(i<15){
			sets.put(i,new ArrayList<Card>());
			i++;
		}

		//fill suit hash map
		runs.put("C", new ArrayList<Card>());
		runs.put("D", new ArrayList<Card>());
		runs.put("H", new ArrayList<Card>());
		runs.put("S", new ArrayList<Card>());
	}

	public void getRuns(ArrayList<Card> cards){
		Collections.sort(cards);
		//make the sets
		int i = 0;
		while(i< cards.size()){
			Card c = cards.get(i);
			String suit = c.getSuit();
			ArrayList<Card> temp = runs.get(suit);
			temp.add(c);
			//System.out.println("Temp" + temp);
			runs.put(suit, temp);
			i++;
			//System.out.println("Run size:" + runs.size());
		}

		String suitString = "";
		int suitNum = 1;
		while(suitNum<5){
			switch(suitNum){
			case 1:
				suitString = "C";break;
			case 2:
				suitString = "D";break;
			case 3:
				suitString = "H";break;
			case 4:
				suitString = "S";break;		
			}

			ArrayList<Card> x = runs.get(suitString);

			if(x!= null){
				//System.out.println("not null");

				if(checkConsecutive((ArrayList<Card>) x) && x.size()>2){
					//computeTotal(x);
					runList.add((ArrayList<Card>) x);
					//System.out.println("Adding to runs");

				}else{
					
					for(Card c: x){
						leftOver.add(c);
						System.out.println("Adding to leftover");

					}
					//leftOver.addAll((ArrayList<Card>) x);
					System.out.println("Adding to leftover");
				}
				suitNum++;
			}

			//return runList;
		}
	}

	/**
	 * Retrieve card data from input
	 * @param String s
	 */
	public void getInput(String s){

		//split input by whitespace
		String[] cards = s.split(" ");

		for(int c = 0; c<cards.length; c++){
			cardList.add(new Card(cards[c]));
			//System.out.println( cards[c].toString());
		}

		Collections.sort(cardList);
		//System.out.println( cardList.toString());
	}

	/**
	 * 
	 * @param cards
	 * @return 2H 3H 4H
	 */
	public void getSets(ArrayList<Card> cards){
		Collections.sort(cards);

		//make the sets
		int i =0;
		while(i< cards.size()){
			Card c = cards.get(i);
			int order = c.getOrder();
			ArrayList<Card> temp = sets.get(order);
			temp.add(c);
			sets.put(order, temp);
			i++;
		}

		for(int j = 1;j < 15; j++){
			List<Card> x = sets.get(j);
			if(x.size()>2){
				setList.add((ArrayList<Card>) x);
			}else{
				leftOver.addAll((ArrayList<Card>) x);
			}
		}

		//return setList;
	}

	//	public List<ArrayList<Card>> getRun(List<Card> c){
	//		Collections.sort(c);
	//		List<Card> cards = new ArrayList<Card>();
	//		cards = c;
	//
	//		List<Card> clubs = new ArrayList<Card>();
	//		List<Card> diamonds = new ArrayList<Card>();
	//		List<Card> hearts = new ArrayList<Card>();
	//		List<Card> spades = new ArrayList<Card>();
	//
	//		runList.add((ArrayList<Card>) clubs);
	//		runList.add((ArrayList<Card>) diamonds);
	//		runList.add((ArrayList<Card>) hearts);
	//		runList.add((ArrayList<Card>) spades);
	//
	//		for(int i=0; i<c.size();i++){
	//			Card temp = cards.get(i);
	//			String x = temp.getSuit();
	//			System.out.println("Suit" + x);
	//
	//			switch(x){
	//			case "C" :
	//				clubs.add(temp);
	//				//cards.remove(i);
	//				break;
	//			case "D" :
	//				diamonds.add(temp);
	//				//cards.remove(i);
	//				break;
	//			case "H":
	//				hearts.add(temp);
	//				//cards.remove(i);
	//				break;
	//			case "S":
	//				spades.add(temp);
	//				//cards.remove(i);
	//				break;
	//
	//			}
	//
	//		} 
	//		leftOver = cards;
	//		runList = addRuns(runList);
	//
	//		return runList;
	//	}


	//	private List<ArrayList<Card>> addRuns(List<ArrayList<Card>> list){
	//		List<ArrayList<Card>> temp = new ArrayList<ArrayList<Card>>();
	//		int i= 0;
	//
	//		//add only non-empty lists
	//		while(i<list.size()){
	//			if(!list.get(i).isEmpty() 
	//					&& checkConsecutive(list.get(i)))
	//				temp.add(list.get(i));
	//			i++;
	//		}
	//
	//		return temp;
	//	}

	private boolean checkConsecutive( ArrayList<Card> suitList){
		Collections.sort(suitList);
		int i =1;
		if(suitList.isEmpty()){
			return false;
		}
		while(i<suitList.size()){
			int dif = (suitList.get(i).getOrder()) - (suitList.get(i-1).getOrder()) ;
			if(dif!= 1){
				return false;
			}
			i++;
		}
		return true;
	}

	/**
	 * Add ea
	 * @param cards
	 * @return
	 */
	private int computeTotal(List<Card> cards){

		int total = 0;
		int y = 0;

		while(y<cards.size()){
			Card temp = cards.get(y);
			total += temp.getOrder();
			y++;

		}
		return 0;

	}
}


