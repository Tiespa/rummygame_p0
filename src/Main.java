import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//package edu.mtholyoke.cs341bd.p0;

public class Main {
  public static void main(String[] args) {
	  Game game = new Game();
    System.out.println("Programming Assignment #0: Rummy Scoring");
    System.out.println("CS341 - Building a Digital Library");
    // TODO adjust your author name.
    System.out.println("Author: Tracy Keya");

    while(true) {
      String input = CLI.readString("Input> ");
      if(input == null || input.length() == 0) {
        break;
      }
      System.out.println("You have entered: ");
      System.out.println("\t"+input);
      System.out.println();
     // game.getInput(input);
      game.start(input);
    }
    
    
    
  }
  
  
}
