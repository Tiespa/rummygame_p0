import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 */

/**
 * @author Tracy
 *
 */
public class CardTest
{
	public List<Card> cards = new ArrayList<Card>();

	//@Before
	public void begin(){
		
	//cards.add(new Card("KS"));
	//cards.add(new Card("10H"));
	//cards.add(new Card("2H"));
	//cards.add(new Card("AH"));
	//cards.add(new Card("5H"));
	//cards.add(new Card("5D"));
	//cards.add(new Card("5S"));


	}
	//@Test
	public void testGetOrder()
	{
		assertNotNull(cards.get(0).getOrder() );
		//assertEquals(cards.get(0).getOrder(),13);
		assertEquals(10,cards.get(0).getOrder());

	}
	
	//@Test
	public void testCharAt(){
		String x = "2S";
		assertEquals('2',x.charAt(0));
	}
	
	//@Test
	public void testsubstringAt(){
		String x = "10S";
		String t = x.substring(0,2);
		System.out.println("x:" + x);
		System.out.println("t:" + t);

		//assertEquals(x,t);
	}
	
	//@Test
	public void testGetSuit()
	{
		assertEquals("H",cards.get(0).getSuit());
		assertEquals("S",cards.get(1).getSuit());

	}
	
	//@Test
	public void testCompareStrings()
	{
		assertEquals("H",cards.get(0).getSuit());
		assertEquals("S",cards.get(1).getSuit());

	}
	
	//@Test
	public void testSort()
	{
		cards.add(new Card("7H"));
		cards.add(new Card("7S"));
		cards.add(new Card("2D"));
		cards.add(new Card("2C"));
		Card.sortCards(cards);
		
		for(int i=0 ; i< cards.size(); i++){
			System.out.println(cards.get(i).toString());

		}
	}
	
	//@Test
	public void testGetRun()
	{
		Game game = new Game();
		cards.add(new Card("7H"));
		cards.add(new Card("6H"));
		cards.add(new Card("5H"));
		cards.add(new Card("4H"));
		cards.add(new Card("AS"));
		cards.add(new Card("9S"));
		cards.add(new Card("10S"));

		System.out.println(cards.size());
		
		//List<ArrayList<Card>> run = game.getRun(cards);
	//	System.out.println(run.toString());
	//	System.out.println(run.size());
	//	System.out.println(game.leftOver.size());


	}
	

	//@Test
	public void testGetSet()
	{
		Game game = new Game();
		cards.add(new Card("7H"));
		cards.add(new Card("7S"));
		cards.add(new Card("7D"));
		cards.add(new Card("8H"));
		cards.add(new Card("2S"));
		cards.add(new Card("3C"));
		cards.add(new Card("10S"));

		System.out.println(cards.size());
		
		//List<ArrayList<Card>> set = game.getSets((ArrayList<Card>) cards);
	//	System.out.println(set.toString());
	//	System.out.println(set.size());
	//	System.out.println(game.leftOver.size());
	//	assertEquals(1,set.size());


	}
	
	//@Test
	public void testGetRunHash()
	{
		Game game = new Game();
		cards.add(new Card("2H"));
		cards.add(new Card("5S"));
		cards.add(new Card("3D"));
		cards.add(new Card("3H"));
		cards.add(new Card("6S"));
		cards.add(new Card("4C"));
		cards.add(new Card("7S"));

		//System.out.println(cards.size());
		//System.out.println(cards);

	//	List<ArrayList<Card>> runs = game.getRuns((ArrayList<Card>) cards);
	//	System.out.println(runs.toString());
	//	assertEquals(1,runs.size());
		//System.out.println(runs.size());
	//	System.out.println(game.leftOver.size());


	}
	
	
	

}
