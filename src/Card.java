import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 */

/**
 * @author Tracy
 *
 */
public class Card implements Comparable<Card>
{
	public Integer order;		/**card num value**/
	public String suit;
	public HashMap<String,Integer> orderMap ; /**store card values**/
	public String name;

	public Card(String c){
			c = c.toUpperCase();
			this.name = c;
			setOrder(c); 
			setSuit(c);
	}
	
	
	/**
	 * 
	 * @param c
	 */
	private void setOrder(String c){
		
		//get only the first letter only else get first two letters
		if(c.length() <= 2 ){
			c = Character.toString(c.charAt(0));
		}else{
			c = c.substring(0,2);
		}
		switch(c){
		case "A":
			this.order = 14;
			break;
		case "K":
			this.order = 13;
			break;
		case "Q":
			this.order = 12;
			break;
		case "J":
			this.order = 11;
			break;
		default:
			this.order = Integer.parseInt(c);
			break;
		}
	
	}
	
	public int getOrder(){
		return this.order;
	}
	
	private int getCost(Card[] cards){
		return 0;
	}
	
	public void setSuit(String c){
		String x =  Character.toString(c.charAt(c.length()-1));
		this.suit = x;
		
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public static void sortCards(List<Card> cards){
		//cards.sort();
		Collections.sort(cards);
	}
	
	public String getPrint(){
		return this.toString();
	}

	@Override
	public int compareTo(Card o)
	{
		if(order.compareTo(o.order)==0){
			return suit.compareTo(o.suit);
		}
		return order.compareTo(o.order);
		
	}

	@Override
	public String toString(){
		return this.name;
	}
	
	private boolean checkConsecutive( ArrayList<Card> suitList){
		int i =1;
		while(i<suitList.size()){
			if((suitList.get(i).getOrder() -suitList.get(i-1).getOrder() != 1 )){
				return false;
			}
			i++;
		}
		return true;
	}
	
	private static List<ArrayList<Card>> addSets(List<ArrayList<Card>> list){
		List<ArrayList<Card>> temp = new ArrayList<ArrayList<Card>>();
		int i= 0;
		
		//add only non-empty lists
		while(i<list.size()){
			if(!list.get(i).isEmpty())
				temp.add(list.get(i));
			i++;
		}
		
		return temp;
	}

	
	
}
